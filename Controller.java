/*
 * Controller.java
 */

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JFileChooser;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * The controller class for the word count application.
 *
 * @author Tou Ko Lee
 */
public class Controller implements ActionListener {

    /**
     * The Model of the word count data.
     */
    private final Model model = new Model();

    /**
     * The Gui for the word count app.
     */
    private final Gui view;

    /**
     * Constructor using the command line only
     *
     * @param file The file name
     */
    public Controller(String file) {
        view = new Gui(null);
        getStats(file);
    }

    /**
     * Constructor using the gui only
     */
    public Controller() {
        view = new Gui(this);
        view.setVisible(true);
    }

    /**
     * Counts the amount of each word and the total word count and stores them
     * within the model Prints out the results in the command line
     *
     * @param name The file name
     */
    public void getStats(String name) {
        System.out.println("The text file did you want to count:");
        System.out.println(name);
        System.out.println();

        // try and catch if user inputed wrong file name or file can't be found
        try {

            File file = new File(name);
            Scanner reader = new Scanner(file);

            while (reader.hasNext()) {
                String word = reader.next();

                //checks if there is quotations and removes if there is
                char firstChar = word.charAt(0);
                if (!Character.isLetter(firstChar)) {
                    if (firstChar == '"'){
                        word = word.substring(1);
                    }
                    // checks if there is any word or if the next letter is a letter
                    String temp = word.substring(1);
                    if (temp.length() == 0 || !Character.isLetter(temp.charAt(0))) {
                        continue;
                    }
                    word = temp;
                }

                word = word.toLowerCase();
                //checks if there is punctuation and removes if there is
                char lastChar = word.charAt(word.length() - 1);
                if (!Character.isLetter(lastChar)) {
                    word = word.substring(0, word.length() - 1);
                }
                //checks if there is quotations and removes if there is
                lastChar = word.charAt(word.length() - 1);
                if (!Character.isLetter(lastChar)) {
                    word = word.substring(0, word.length() - 1);
                }

                //updates total words
                model.setTotal(model.getTotal() + 1);

                //checks if the word is in the HashMap else updates the value by 1
                if (!model.getWords().containsKey(word)) {
                    model.getWords().put(word, 1);
                } else {
                    Integer value = model.getWords().get(word);
                    value++;
                    model.getWords().put(word, value);
                }
            }

            //prints out results
            System.out.println("Total words:\n" + model.getTotal());
            
            String words = "Heres the list of words found and the number:\n";
            
            //sorts words into alphabetic order
            Map<String, Integer> sortedWords = new TreeMap<>(model.getWords());
            //adds new words to the string
            for (Map.Entry<String, Integer> entry : sortedWords.entrySet()) {
                words += entry.getKey() + " " + entry.getValue() + "\n";
            }
            
            //writes the words to a new file
            String newFile = name.replace(".", "Stats.");
            Path fileStats = Path.of(newFile);
            try {
                Files.writeString(fileStats, words);
            } catch (IOException ex) {
                System.out.println(ex);
            }
            

        } catch (FileNotFoundException e) {
            System.out.println("File not found");
            e.printStackTrace();
        }
    }

    /**
     * Counts the amount of each word and the total word count and stores them
     * within the model Prints out the results in the command line
     *
     * @param name The file
     */
    public void getStats(File name) {
        // try and catch if user inputed wrong file name or file can't be found
        try {
            Scanner reader = new Scanner(name);

            while (reader.hasNext()) {
                String word = reader.next();

                //checks if there is quotations and removes if there is
                char firstChar = word.charAt(0);
                if (!Character.isLetter(firstChar)) {
                    if (firstChar == '"'){
                        word = word.substring(1);
                    }
                    // checks if there is any word or if the next letter is a letter
                    String temp = word.substring(1);
                    if (temp.length() == 0 || !Character.isLetter(temp.charAt(0))) {
                        continue;
                    }
                    word = temp;
                }

                word = word.toLowerCase();
                //checks if there is punctuation and removes if there is
                char lastChar = word.charAt(word.length() - 1);
                if (!Character.isLetter(lastChar)) {
                    word = word.substring(0, word.length() - 1);
                }
                //checks if there is quotations and removes if there is
                lastChar = word.charAt(word.length() - 1);
                if (!Character.isLetter(lastChar)) {
                    word = word.substring(0, word.length() - 1);
                }

                //updates total words
                model.setTotal(model.getTotal() + 1);

                //checks if the word is in the HashMap else updates the value by 1
                if (!model.getWords().containsKey(word)) {
                    model.getWords().put(word, 1);
                } else {
                    Integer value = model.getWords().get(word);
                    value++;
                    model.getWords().put(word, value);
                }
            }
            
            String words = "Heres the list of words found and the number:\n";
            
            //sorts words into alphabetic order
            Map<String, Integer> sortedWords = new TreeMap<>(model.getWords());
            //adds new words to the string
            for (Map.Entry<String, Integer> entry : sortedWords.entrySet()) {
                words += entry.getKey() + " " + entry.getValue() + "\n";
            }
            
            //writes the words to a new file
            String newFile = name.getName().replace(".", "Stats.");
            Path fileStats = Path.of(newFile);
            try {
                Files.writeString(fileStats, words);
            } catch (IOException ex) {
                System.out.println(ex);
            }
            
        } catch (FileNotFoundException e) {
            System.out.println("File not found");
            e.printStackTrace();
        }
    }

    /**
     * Counts the amount of each word and the total word count and stores them
     * within the model Prints out the results in the command line
     *
     * @param ae The event which occurred, identifying which button was pushed.
     */
    @Override
    public void actionPerformed(ActionEvent ae) {
        if (ae.getActionCommand().equals("Browse")) {
            JFileChooser chooser = new JFileChooser();
            chooser.showOpenDialog(null);
            model.setFile(chooser.getSelectedFile());
            this.view.fileName.setText(model.getFile().getName());
        } else if (ae.getActionCommand().equals("run")) {
            String fileName = this.view.fileName.getText();
            // nothing happens if there are no fileName or fileName is wrong text file
            if (fileName.equals("")) {
                System.out.println("There is no file.");
                return;
            }
            if (fileName.indexOf(".txt") == -1) {
                System.out.println("Wrong file type. Only .txt files!");
                return;
            }
            getStats(model.getFile());

            //sets the view to have the stats
            this.view.totalWordsText.setText(String.valueOf(model.getTotal()));

            Map<String, Integer> sortedWords = new TreeMap<>(model.getWords());
            String words = "";
            for (Map.Entry<String, Integer> entry : sortedWords.entrySet()) {
                words += entry.getKey() + " " + entry.getValue() + "\n";
            }
            this.view.wordsText.setText(words);
        }
    }
}
