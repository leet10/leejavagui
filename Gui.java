/*
 * Gui.java
 */

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.ScrollPaneConstants;

/**
 * The word count application graphical user interface. An instance of this gui
 * contains a reference to the Controller.
 *
 * @author Tou Ko Lee
 */
public class Gui extends JFrame {

    private final JLabel file = new JLabel("File: ");
    private final JLabel totalWords = new JLabel("Total words: ");
    private final JLabel words = new JLabel("Words found: ");
    private final JButton browse = new JButton("Browse");
    private final JButton run = new JButton("run");
    protected final JTextField fileName = new JTextField();
    protected final JTextField totalWordsText = new JTextField();
    protected final JTextArea wordsText = new JTextArea();

    public Gui(Controller controller) {
        this.setTitle("Word Count App");
        this.setSize(360, 520);
        this.setLocation(100, 100);
        this.getContentPane().setLayout(null);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setResizable(false);

        this.browse.setLocation(50, 100);
        this.browse.setSize(100, 50);
        this.browse.addActionListener(controller);
        this.getContentPane().add(this.browse);

        this.run.setLocation(200, 100);
        this.run.setSize(100, 50);
        this.run.addActionListener(controller);
        this.getContentPane().add(this.run);

        this.file.setBounds(20, 50, 100, 30);
        this.getContentPane().add(this.file);

        this.fileName.setBounds(100, 50, 200, 30);
        this.fileName.setEditable(false);
        this.getContentPane().add(fileName);

        this.totalWords.setBounds(20, 170, 80, 30);
        this.getContentPane().add(this.totalWords);

        this.totalWordsText.setBounds(100, 170, 200, 30);
        this.totalWordsText.setEditable(false);
        this.getContentPane().add(totalWordsText);

        this.words.setBounds(20, 230, 80, 30);
        this.getContentPane().add(this.words);

        this.wordsText.setEditable(false);
        JScrollPane scroll = new JScrollPane(this.wordsText);
        scroll.setBounds(100, 230, 200, 200);
        this.getContentPane().add(scroll);
    }
}
