Summary:
The application will read in and count all the occurrences of each word. The application will then print out to a file the total count of words along with all the counts of the separate words, doing so in dictionary order.

Organization:
The application will consist of 4 java files, Main.java, Controller.java, Gui.java, and Model.java. Main.java will be the driver class where it calls Controller.java. Controller.java will use the information from Model.java to set and get the stats about the text file. Model.java will contain all the variables I will use in the application. Gui.java will be the physical representation where the user will interact with.

How-to-run section:
Command Line:
The user will compile main.java which would compile the other .java files (Otherwise you may need to compile them too). Then the user will run Main.java with the text file and the application will print out the stats. If there is text file, Gui.java will run and the user will be able to interact with it to select their text file to get the stats found in the summary.
IDE:
Run Main.java and Gui.java will run and the user will be able to interact with it to select their text file to get the stats found in the summary.