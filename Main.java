/*
 * Main.java
 * Driver class for the word count application
 */


/**
 * Driver class for the word count application
 * @author Tou Ko Lee
 */
public class Main{

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        if (args.length != 0){
            Controller c = new Controller(args[0]);
        }
        else{
            Controller c = new Controller();
        }
        
    }
    
}
