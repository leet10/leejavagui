/*
 * Model.java
 */


import java.io.File;
import java.util.HashMap;

/**
 * Information about one file
 * Stores and retrieves the number of total words and the different words
 * @author Tou Ko Lee
 */
public class Model {
    // Stores total words found in the file
    private int totalWords = 0;
    
    // Stores the words found in the file in a HashMap
    private HashMap<String, Integer> words = new HashMap<String, Integer>();
    
    // Stores the file for the Gui to access
    private File file;
    
    /**
     * Gets the total number of words found in the file
     * @return The total number of words
     */
    public int getTotal(){
        return this.totalWords;
    }
    
    /**
     * Gets the words found in the file 
     * @return The words
     */
    public HashMap<String, Integer> getWords(){
        return this.words;
    }
    
    /**
     * Gets the file
     * @return The file
     */
    public File getFile(){
        return this.file;
    }
    
    /**
     * Sets the total number of words found in the file
     * @param n The total number of words
     */
    public void setTotal(int n){
        this.totalWords = n;
    }
    
    /**
     * Sets the words found in the file 
     * @param m The HashMap of words
     */
    public void setWords(HashMap<String, Integer> m){
        this.words = m;
    }
    
    /**
     * Sets the file
     * @param f The file
     */
    public void setFile(File f){
        this.file = f;
    }
}
